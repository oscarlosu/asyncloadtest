﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsyncBundleCallbackLoader : MonoBehaviour {
    public string bundlePath;
    AssetBundleCreateRequest assetBundleCreateRequest;
    AssetBundleRequest assetBundleRequest;
    // Use this for initialization
    void Start () {
        assetBundleCreateRequest = AssetBundle.LoadFromFileAsync(bundlePath);
        assetBundleCreateRequest.completed += LoadFromFileAsync_completed;
    }

    private void LoadFromFileAsync_completed(AsyncOperation obj)
    {
        Debug.Log("AssetBundle.LoadFromFileAsync completed");
        assetBundleRequest = assetBundleCreateRequest.assetBundle.LoadAllAssetsAsync();
        assetBundleRequest.completed += LoadAllAssetsAsync_completed;
    }

    private void LoadAllAssetsAsync_completed(AsyncOperation obj)
    {
        Debug.Log("AssetBundle.LoadAllAssetsAsync completed");        
    }

    private void OnDestroy()
    {
        assetBundleCreateRequest.assetBundle.Unload(true);
    }
}
