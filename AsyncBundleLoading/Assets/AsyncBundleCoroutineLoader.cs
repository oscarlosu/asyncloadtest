﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AsyncBundleCoroutineLoader : MonoBehaviour {
    public string bundlePath;
    public Text text;
    AssetBundle assetBundle;

    void Start () {
        StartCoroutine(Load());
    }

    IEnumerator Load()
    {
        AssetBundleCreateRequest assetBundleCreateRequest = AssetBundle.LoadFromFileAsync(bundlePath);
        while(!assetBundleCreateRequest.isDone)
        {
            text.text = "LoadFromFileAsync progress: " + (assetBundleCreateRequest.progress * 100.0f) + "%";
            yield return null;
        }
        Debug.Log("AssetBundle.LoadFromFileAsync completed");
        assetBundle = assetBundleCreateRequest.assetBundle;
        AssetBundleRequest assetBundleRequest = assetBundle.LoadAllAssetsAsync();
        while (!assetBundleRequest.isDone)
        {
            text.text = "LoadAllAssetsAsync progress: " + (assetBundleRequest.progress * 100.0f) + "%";
            yield return null;
        }
        text.text = "Loading completed";
        Debug.Log("AssetBundle.LoadAllAssetsAsync completed");
    }

    private void OnDestroy()
    {
        assetBundle.Unload(true);
    }
}
